# Boot animation
TARGET_SCREEN_WIDTH := 800
TARGET_SCREEN_HEIGHT := 1280

# Inherit some common CM stuff.
$(call inherit-product, vendor/cm/config/common_full_phone.mk)

# Inherit device configuration
$(call inherit-product, device/lenovo/YT3X50M/yt3x50m.mk)

# Device identifier. This must come after all inclusions
PRODUCT_DEVICE := YT3X50M
PRODUCT_NAME := cm_YT3X50M
PRODUCT_BRAND := Lenovo
PRODUCT_MODEL := YT3X50M
PRODUCT_MANUFACTURER := LENOVO
PRODUCT_RELEASE_NAME := LMY47V

# TODO:
# Set build fingerprint / ID / Product Name ect.
#PRODUCT_BUILD_PROP_OVERRIDES += \
#    PRIVATE_BUILD_DESC="Pop35-user 5.1.1 LMY47V vA73-0 release-keys" \
#    BUILD_FINGERPRINT="Lenovo/YT3-X50M/YT3-X50M:5.1.1/LMY47V/YT3-X50M_USR_S024_160614_Q1241_ROW:user/release-keys"
##    BUILD_FINGERPRINT="TCL/5065D/Pop35:5.1.1/LMY47V/vA73-0:user/release-keys"

PRODUCT_GMS_CLIENTID_BASE := android-lenovo

# TODO: 
# Build signing
#ifneq ($(wildcard vendor/paz00/cert/releasekey*),)
#	PRODUCT_DEFAULT_DEV_CERTIFICATE := vendor/paz00/cert/releasekey
#endif
